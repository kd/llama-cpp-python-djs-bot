#!/bin/bash
# This is the main process of the container and will stay alive as long as pm2 log is running.

pm2 start startServices.json

pm2 log